# -*- coding: utf-8 -*-

import random
from copy import deepcopy
import numpy as np
import matplotlib.pyplot as plt
# import pandas as pd

import CostFunction

# ensure repeatability
random.seed(4)

class Protocol:
    def __init__(self, problem, protocolType, specialHouse=None):

        self.specialHouse = False
        if specialHouse:
            problem.addHouse(specialHouse)
            self.specialHouse = True

        # PARAMETERS
        self.problem = problem
        self.protocolType = protocolType

        # VARIABLES TO HOLD
        # self.numberOfPriceIncreases = 0
        # self.totalConsumption = 0
        # self.averagePrice = 0
        # self.consumptionFinal = []
        # self.priceFinal = []
        # self.protocolLength = 0
        # self.socialWelfare = 0


    def run(self):
        prices = [0 for _ in xrange(len(self.problem.houseList))]
        counterINCtotal = 0
        protocolLength = 0
        consumptionVectorGet = []

        old_consumption = []
        new_consumption = []

        while True:
            consumptionVectorGet[:] = []
            totalConsumptionHouses = 0
            for i, house in enumerate(self.problem.houseList):
                consumption, counterINC = house.reportConsumptionNegotiation(prices[i], 0)
                consumptionVectorGet.append(consumption)
                totalConsumptionHouses += consumptionVectorGet[-1]
                counterINCtotal += counterINC

            old_consumption[:], new_consumption[:] = new_consumption, consumptionVectorGet

            if end_protocol(old_consumption, new_consumption):
                for house in self.problem.houseList:
                    house.setSatisfaction()
                break

                # CHOOSE DIFFERENT PRICING MECHANISMS
            if self.protocolType == 'serial':
                prices = self.problem.negotiator.computePricesNegotiation(consumptionVectorGet)
            if self.protocolType == 'average':
                prices = self.problem.negotiator.computePricesAverage(consumptionVectorGet)
            if self.protocolType == 'tarrifs':
                prices = self.problem.negotiator.computePricesTarrifs(consumptionVectorGet)
            protocolLength += 1

        self.protocolLength = protocolLength
        self.numberOfPriceIncreases = counterINCtotal

        consumptionFinal = []
        priceFinal = []
        valueFinal = []
        allCosts = []
        allSatisfactions = []
        allSatisfactions2 = []
        for house in self.problem.nonGamblers:
            consumptionFinal.append(house.currentConsumption)
            priceFinal.append(house.priceTrack[-1])
            valueFinal.append(house.currentMaxPrice)
            allCosts.append(consumptionFinal[-1] * priceFinal[-1])
            allSatisfactions.append(house.isSatisfied * consumptionFinal[-1])
            allSatisfactions2.append(house.isSatisfied2)

        self.consumptionFinal = consumptionFinal
        self.priceFinal = priceFinal
        self.valueFinal = valueFinal
        self.allCosts = allCosts
        self.allSatisfactions = allSatisfactions
        self.allSatisfactions2 = allSatisfactions2

        consumptionFinal_gamblers = []
        valueFinal_gamblers = []
        priceFinal_gamblers = []
        allCosts_gamblers = []
        allSatisfactions_gamblers = []
        allSatisfactions2_gamblers = []
        for house in self.problem.gamblers:
            consumptionFinal_gamblers.append(house.currentConsumption)
            valueFinal_gamblers.append(house.currentMaxPrice)
            priceFinal_gamblers.append(house.priceTrack[-1])
            allCosts_gamblers.append(consumptionFinal_gamblers[-1] * priceFinal_gamblers[-1])
            allSatisfactions_gamblers.append(house.isSatisfied * consumptionFinal_gamblers[-1])
            allSatisfactions2_gamblers.append(house.isSatisfied2)

        self.consumptionFinal_gamblers = consumptionFinal_gamblers
        self.priceFinal_gamblers = priceFinal_gamblers
        self.valueFinal_gamblers = valueFinal_gamblers
        self.allCosts_gamblers = allCosts_gamblers
        self.allSatisfactions_gamblers = allSatisfactions_gamblers
        self.allSatisfactions2_gamblers = allSatisfactions2_gamblers


    # funkcija za izracun nasha (skoraj), ce bi zeleli tocno nasha, bi morali izvesti več rund in v vsaki rundi.
    # v eni rundi bi dobili porabo za hiso, ki najmanj pokuri, to porabo bi nato fiksirali ter iskali porabe ostalih
    def nash_equilibrium(self):
        nr_of_houses = len(self.problem.houseList)

        all_consumptions = []

        # za vsako hiso izracunamo najboljsi odgovor v najslabsem primeru (max min)
        # min, ker hisa predvideva najvecje stroske za porabo
        # max, ker isce porabo, ki prinese najvecje zadovoljstvo (ob predpostavki najslabsega primera - min)
        # hisa potrebuje cost funkcijo, da se lahko odloci
        for house in self.problem.houseList:
            best_cons_level = 0 # stopnja porabe, 0-ne porabi nic, 1-porabi p1, 2-porabi p2 ...
            best_utility = 0 # zadovoljstvo
            for cons_level in range(len(house.devicesConsumption)+1):
                consumptionVectorGet = nr_of_houses * [sum(house.devicesConsumption[:cons_level])] # worst case za hiso, vsi porabijo enako

                prices = self.problem.negotiator.computePricesAverage(consumptionVectorGet) # ce delamo vec rund in iscemo pravo nashevo ravnovesje, je treba uporabiti serial cost sharing

                utility = house.getSatisfaction(sum(house.devicesConsumption[:cons_level]), prices[0]) # izracun zadovoljstva

                if utility > best_utility: # isci max
                    best_cons_level = cons_level
                    best_utility = utility

            house.consumptionLevel = best_cons_level    # shrani odlovitev
            house.currentMaxPrice = house.devicesPricesNegotiation[best_cons_level - 1] if best_cons_level > 0 else 0
            house.currentConsumption = sum(house.devicesConsumption[:best_cons_level])
            house.consumptionTrack.append(house.currentConsumption)

            all_consumptions.append(house.currentConsumption)


        # izracun dejanskih cen po koncu "pogajanj"
        if self.protocolType == 'serial':
            prices = self.problem.negotiator.computePricesNegotiation(all_consumptions)
        if self.protocolType == 'average':
            prices = self.problem.negotiator.computePricesAverage(all_consumptions)
        if self.protocolType == 'tarrifs':
            prices = self.problem.negotiator.computePricesTarrifs(all_consumptions)

        for idx, house in enumerate(self.problem.houseList):
            house.priceTrack.append(prices[idx])
            house.setSatisfaction()


        ###############################################
        ###### SAVE STUFF #############################
        ###############################################

        self.protocolLength = 0
        self.numberOfPriceIncreases = 0

        consumptionFinal = []
        priceFinal = []
        valueFinal = []
        allCosts = []
        allSatisfactions = []
        allSatisfactions2 = []
        for house in self.problem.nonGamblers:
            consumptionFinal.append(house.currentConsumption)
            priceFinal.append(house.priceTrack[-1])
            valueFinal.append(house.currentMaxPrice)
            allCosts.append(consumptionFinal[-1] * priceFinal[-1])
            allSatisfactions.append(house.isSatisfied * consumptionFinal[-1])
            allSatisfactions2.append(house.isSatisfied2)

        self.consumptionFinal = consumptionFinal
        self.priceFinal = priceFinal
        self.valueFinal = valueFinal
        self.allCosts = allCosts
        self.allSatisfactions = allSatisfactions
        self.allSatisfactions2 = allSatisfactions2

        consumptionFinal_gamblers = []
        valueFinal_gamblers = []
        priceFinal_gamblers = []
        allCosts_gamblers = []
        allSatisfactions_gamblers = []
        allSatisfactions2_gamblers = []
        for house in self.problem.gamblers:
            consumptionFinal_gamblers.append(house.currentConsumption)
            valueFinal_gamblers.append(house.currentMaxPrice)
            priceFinal_gamblers.append(house.priceTrack[-1])
            allCosts_gamblers.append(consumptionFinal_gamblers[-1] * priceFinal_gamblers[-1])
            allSatisfactions_gamblers.append(house.isSatisfied * consumptionFinal_gamblers[-1])
            allSatisfactions2_gamblers.append(house.isSatisfied2)

        self.consumptionFinal_gamblers = consumptionFinal_gamblers
        self.priceFinal_gamblers = priceFinal_gamblers
        self.valueFinal_gamblers = valueFinal_gamblers
        self.allCosts_gamblers = allCosts_gamblers
        self.allSatisfactions_gamblers = allSatisfactions_gamblers
        self.allSatisfactions2_gamblers = allSatisfactions2_gamblers

    def all_rounds_nash_equilibrium(self):
        nr_of_houses = len(self.problem.houseList)


        # za vsako hiso izracunamo najboljsi odgovor v najslabsem primeru (max min)
        # min, ker hisa predvideva najvecje stroske za porabo
        # max, ker isce porabo, ki prinese najvecje zadovoljstvo (ob predpostavki najslabsega primera - min)
        # hisa potrebuje cost funkcijo, da se lahko odloci

        remaining_houses = zip(range(nr_of_houses), deepcopy(self.problem.houseList))
        processed_houses = []

        all_consumptions = []
        set_consumptions = []
        for house_idx in xrange(nr_of_houses):
            #print house_idx
            all_consumptions[:] = []
            set_consumptions[:] = []
            for house in processed_houses:
                all_consumptions.append(house)
                set_consumptions.append(house[1])

            for temp_ix, hh in enumerate(remaining_houses):
                h_ix, house = hh
                best_cons_level = 0  # stopnja porabe, 0-ne porabi nic, 1-porabi p1, 2-porabi p2 ...
                best_utility = 0  # zadovoljstvo
                best_price = 0
                consumptionVectorGet = []
                for cons_level in range(len(house.devicesConsumption)+1):
                    consumptionVectorGet[:] = set_consumptions + (nr_of_houses - len(set_consumptions)) * [sum(house.devicesConsumption[:cons_level])]  # worst case za hiso, vsi porabijo enako

                    # prices = self.problem.negotiator.computePricesAverage(consumptionVectorGet) # ce delamo vec rund in iscemo pravo nashevo ravnovesje, je treba uporabiti serial cost sharing
                    # utility = house.getSatisfaction(sum(house.devicesConsumption[:cons_level]), prices[0]) # izracun zadovoljstva
                    #print consumptionVectorGet
                    #print len(consumptionVectorGet)
                    price = self.problem.negotiator.computePricesNegotiation(consumptionVectorGet)[-1]
                    utility = house.getSatisfaction(sum(house.devicesConsumption[:cons_level]), price)
                    if utility > best_utility:  # isci max
                        best_cons_level = cons_level
                        best_utility = utility
                        best_price = price

                house.consumptionLevel = best_cons_level    # shrani odlovitev
                house.currentMaxPrice = house.devicesPricesNegotiation[best_cons_level - 1] if best_cons_level > 0 else 0
                house.currentConsumption = sum(house.devicesConsumption[:best_cons_level])
                house.consumptionTrack.append(house.currentConsumption)

                all_consumptions.append((h_ix, house.currentConsumption, house.currentMaxPrice, house.consumptionTrack, house.consumptionLevel, temp_ix, best_price))

            #print "\n\nAll remaining ", all_consumptions
            processed_houses.append(min(all_consumptions[len(processed_houses):], key=lambda x: x[1]))
            #print "Processed ", processed_houses
            #print "Remaining ",
            del remaining_houses[processed_houses[-1][5]]

        for house in processed_houses:
            house_from_list = self.problem.houseList[house[0]]
            house_from_list.currentConsumption = house[1]
            house_from_list.currentMaxPrice = house[2]
            house_from_list.consumptionTrack = house[3]
            house_from_list.consumptionLevel = house[4]
            house_from_list.expected_price = house[6]


        best_prices = []
        for house in self.problem.houseList:
            best_prices.append(house.expected_price)

        all_consumptions[:] = []
        for house in self.problem.houseList:
            all_consumptions.append(house.currentConsumption)

        # izracun dejanskih cen po koncu "pogajanj"
        if self.protocolType == 'serial':
            prices = self.problem.negotiator.computePricesNegotiation(all_consumptions)
        if self.protocolType == 'average':
            prices = self.problem.negotiator.computePricesAverage(all_consumptions)
        if self.protocolType == 'tarrifs':
            prices = self.problem.negotiator.computePricesTarrifs(all_consumptions)

        for idx, house in enumerate(self.problem.houseList):
            house.priceTrack.append(prices[idx])
            house.setSatisfaction()

        #razlike_prices = [abs(bp - nw) for bp, nw in zip(best_prices, prices)]
        #print np.mean(razlike_prices)
        #plt.hist(razlike_prices)
        #plt.show()

        # plt.plot(prices, label="True prices")
        # plt.plot(best_prices, label="Expected prices")
        # plt.legend()
        # plt.show()

        ###############################################
        ###### SAVE STUFF #############################
        ###############################################

        self.protocolLength = 0
        self.numberOfPriceIncreases = 0

        consumptionFinal = []
        priceFinal = []
        valueFinal = []
        allCosts = []
        allSatisfactions = []
        allSatisfactions2 = []
        for house in self.problem.nonGamblers:
            consumptionFinal.append(house.currentConsumption)
            priceFinal.append(house.priceTrack[-1])
            valueFinal.append(house.currentMaxPrice)
            allCosts.append(consumptionFinal[-1] * priceFinal[-1])
            allSatisfactions.append(house.isSatisfied * consumptionFinal[-1])
            allSatisfactions2.append(house.isSatisfied2)

        self.consumptionFinal = consumptionFinal
        self.priceFinal = priceFinal
        self.valueFinal = valueFinal
        self.allCosts = allCosts
        self.allSatisfactions = allSatisfactions
        self.allSatisfactions2 = allSatisfactions2

        consumptionFinal_gamblers = []
        valueFinal_gamblers = []
        priceFinal_gamblers = []
        allCosts_gamblers = []
        allSatisfactions_gamblers = []
        allSatisfactions2_gamblers = []
        for house in self.problem.gamblers:
            consumptionFinal_gamblers.append(house.currentConsumption)
            valueFinal_gamblers.append(house.currentMaxPrice)
            priceFinal_gamblers.append(house.priceTrack[-1])
            allCosts_gamblers.append(consumptionFinal_gamblers[-1] * priceFinal_gamblers[-1])
            allSatisfactions_gamblers.append(house.isSatisfied * consumptionFinal_gamblers[-1])
            allSatisfactions2_gamblers.append(house.isSatisfied2)

        self.consumptionFinal_gamblers = consumptionFinal_gamblers
        self.priceFinal_gamblers = priceFinal_gamblers
        self.valueFinal_gamblers = valueFinal_gamblers
        self.allCosts_gamblers = allCosts_gamblers
        self.allSatisfactions_gamblers = allSatisfactions_gamblers
        self.allSatisfactions2_gamblers = allSatisfactions2_gamblers

    def reset_variables(self):
        """ Reset all changeable values that are currently held in the instance.
        :return:
        """

        self.protocolLength = 0
        self.numberOfPriceIncreases = 0

        self.consumptionFinal = []
        self.priceFinal = []
        self.valueFinal = []
        self.allCosts = []
        self.allSatisfactions = []

        self.consumptionFinal_gamblers = []
        self.priceFinal_gamblers = []
        self.valueFinal_gamblers = []
        self.allCosts_gamblers = []
        self.allSatisfactions_gamblers = []

        for house in self.problem.houseList:
            house.reset()


    def run_renunciation(self):
        pass

    def plotRun(self):
        plt.scatter(self.consumptionFinal, self.priceFinal)
        plt.xlabel('Consumption', fontsize=14)
        plt.ylabel('Prices', fontsize=14)
        plt.title(self.protocolType, fontsize=14)
        plt.show()

def end_protocol(old_cons, new_cons):
    if not old_cons:
        # this is the first round of price search
        return False
    else:
        for idx, value in enumerate(old_cons):
            if value != new_cons[idx]:
                # consumption of at least one house was lowered, therefore, continue price searching
                return False
        return True

# protocol_instance = Protocol(Problem(), "serial")
# protocol_instance.run()
# protocol_instance.plotRun()
