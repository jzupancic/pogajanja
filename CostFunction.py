__author__ = 'JernejZ'

# ==============================================================================
#
# def fill(amount, bucketSizes):
# filledBuckets = [0 for _ in bucketSizes]
#
#     index = 0
#     while amount > 0:
#         filledBuckets[index] = min(amount, bucketSizes[index])
#
#     return filledBuckets
#==============================================================================

class CostFunction:
    """
    This object stores the information about cost function.
    """

    def __init__(self, blockSizeList, blockPriceList, numberOfTarrifs=4):
        """
        CostFunction(blockSizeList, blockPriceList)

        Resource is distributed in blocks. Sizes are defined in blockSizeList
        and prices are defined in blockPriceList.
        """
        self.blockSizes = blockSizeList
        self.blockPrices = blockPriceList

        self.numberOfTarrifs = numberOfTarrifs

    def computeSingleCost(self, consumption):
        """
        computeSingleCost(self, consumption)
        Method that computes the cost of providing consumption amount of
        resource.
        """
        remainingConsumption = consumption
        totalCost = 0

        index = 0
        while remainingConsumption > 0:
            reduction = min(remainingConsumption, self.blockSizes[index])
            totalCost += reduction * self.blockPrices[index]
            remainingConsumption -= reduction
            index += 1

        return totalCost

    def computeMultipleCost(self, consumptionVector):
        """
        Method that computes cost for multiple consumers, where consumption
        is given in list consumptionVector.
        """
        enumeratedConsumption = list(enumerate(consumptionVector))
        sortedConsumption = sorted(enumeratedConsumption, key=lambda consumptionPair: consumptionPair[1])

        indexes, consumptionsCF = zip(*sortedConsumption)

        numberOfConsumers = len(consumptionVector)

        costVector = [0 for _ in consumptionVector]
        costVector[0] = 1.0 * self.computeSingleCost(numberOfConsumers * consumptionsCF[0]) / numberOfConsumers

        for i in range(numberOfConsumers)[1:]:
            costVector[i] = costVector[i - 1] + 1.0 * (
                self.computeSingleCost(sum(consumptionsCF[:i]) + (numberOfConsumers - i) * consumptionsCF[i]) - sum(
                    costVector[:i - 1]) - costVector[i - 1] * (numberOfConsumers - i + 1)) / (numberOfConsumers - i)

        indexedCostVector = zip(costVector, indexes)
        sortedCostVector = sorted(indexedCostVector, key=lambda consPair: consPair[1])
        costVector, indexes = zip(*sortedCostVector)

        return list(costVector)

    def computeMultiplePrice(self, consumptionVector):
        cost = self.computeMultipleCost(consumptionVector)
        priceVector = []
        for i, costOne in enumerate(cost):
            if consumptionVector[i] != 0:
                priceVector.append(1.0 * costOne / consumptionVector[i])
            else:
                priceVector.append(0)
        return priceVector

    def computeMultiplePriceAverage(self, consumptionVector):
        priceAverage = self.computeSingleCost(sum(consumptionVector)) / sum(consumptionVector) if sum(consumptionVector) != 0 else 0

        priceVector = [priceAverage for _ in consumptionVector]

        for i in xrange(len(consumptionVector)):
            if consumptionVector[i] == 0:
                priceVector[i] = 0
        return priceVector

    def computeMultiplePriceAverageTarrifs(self, consumptionVector):
        numberOfConsumers = len(consumptionVector)
        numberOfTarrifs = self.numberOfTarrifs

        enumeratedConsumption = list(enumerate(consumptionVector))
        sortedConsumption = sorted(enumeratedConsumption, key=lambda consumptionPair: consumptionPair[1])
        indexes, consumptionsCF = zip(*sortedConsumption)

        tarrifSize = numberOfConsumers if numberOfTarrifs==0 else numberOfConsumers / numberOfTarrifs
        consumptionForTarrifs = [0 for _ in range(numberOfTarrifs)]

        partitionToTarrifs = [nrB * tarrifSize for nrB in range(numberOfTarrifs)]
        partitionToTarrifs += [numberOfConsumers]

        for tarrifIndex in range(len(consumptionForTarrifs)):
            consumptionForTarrifs[tarrifIndex] += sum(
                consumptionsCF[partitionToTarrifs[tarrifIndex]:partitionToTarrifs[tarrifIndex + 1]])

        priceVectorForTarrifs = self.computeMultiplePrice(consumptionForTarrifs)

        priceVector = [0 for _ in consumptionVector]
        for i, priceTarrif in enumerate(priceVectorForTarrifs):
            for houseIndex in xrange(partitionToTarrifs[i], partitionToTarrifs[i + 1]):
                priceVector[houseIndex] = priceTarrif
                if consumptionsCF[houseIndex] == 0:
                    priceVector[houseIndex] = 0

        indexedPriceVector = zip(priceVector, indexes)
        sortedPriceVector = sorted(indexedPriceVector, key=lambda consPair: consPair[1])
        priceVector, indexes = zip(*sortedPriceVector)

        return list(priceVector)

#cff = CostFunction([3,2,4],[1,2,3],2)

#cff.computeMultiplePriceAverageTarrifs([1,1,1])