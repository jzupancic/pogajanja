__author__ = 'JernejZ'

from itertools import chain
import multiprocessing as mp
import sys

from numpy import mean, std, nan
from pymongo import MongoClient
import seaborn

from protocol import Protocol
from NegotiationProblem import Problem

import matplotlib.pyplot as plt
import random

def runExperimentSingle(settings):
    numberOfExperimentRuns, numberOfHouses, numberOfTarrifs, meanGamblingCoefficient, protocolType, number_of_gamblers = settings
    problemNumber = 0

    numberOfPriceIncreases_together = []
    length_together = []

    averageConsumption_together = []
    stdConsumption_together = []
    averagePrice_together = []
    stdPrice_together = []
    averageSocialWelfare_together = []
    stdSocialWelfare_together = []
    averageSocialWelfare2_together = []
    stdSocialWelfare2_together = []

    averageConsumption_gamblers_together = []
    stdConsumption_gamblers_together = []
    averagePrice_gamblers_together = []
    stdPrice_gamblers_together = []
    averageSocialWelfare_gamblers_together = []
    stdSocialWelfare_gamblers_together = []
    averageSocialWelfare2_gamblers_together = []
    stdSocialWelfare2_gamblers_together = []

    # should i write a single run into the database?
    single_write = False

    # return anything? or just do the proccessing and store results in the DB
    return_any = False

    # start a mongo client in order to write to the database
    client = MongoClient(host='jernejzupancicpc.ijs.si', port=27017)
    db = client['results_db_social2_nash']

    # should i do hundreds of experiments and then only return the averages? or should i
    # return every run

    posts_list = []
    for experimentRun in xrange(numberOfExperimentRuns):

        # define the protocol to be run
        #print "loading problem..."
        protocolInstance = Protocol(Problem(numberOfHouses=numberOfHouses, numberOfTarrifs=numberOfTarrifs,
                                            meanGamblingCoefficient=meanGamblingCoefficient, nrOfGamblers=number_of_gamblers), protocolType)

        #-------------------------------------------
        # run the protocol
        #print "running protocol..."
        protocolInstance.nash_equilibrium()
        #protocolInstance.run()
        #-------------------------------------------


        # store some statistics
        numberOfPriceIncreases_together.append(protocolInstance.numberOfPriceIncreases)
        length_together.append(protocolInstance.protocolLength)

        # for neutral houses
        averageConsumption_together.append(mean(protocolInstance.consumptionFinal))
        stdConsumption_together.append(std(protocolInstance.consumptionFinal))

        averagePrice_together.append(mean(protocolInstance.priceFinal))
        stdPrice_together.append(std(protocolInstance.priceFinal))

        averageSocialWelfare_together.append(mean(protocolInstance.allSatisfactions))
        stdSocialWelfare_together.append(std(protocolInstance.allSatisfactions))

        averageSocialWelfare2_together.append(mean(protocolInstance.allSatisfactions2))
        stdSocialWelfare2_together.append(std(protocolInstance.allSatisfactions2))

        # for gamblers
        if len(protocolInstance.problem.gamblers) > 0:
            averageConsumption_gamblers_together.append(mean(protocolInstance.consumptionFinal_gamblers))
            stdConsumption_gamblers_together.append(std(protocolInstance.consumptionFinal_gamblers))

            averagePrice_gamblers_together.append(mean(protocolInstance.priceFinal_gamblers))
            stdPrice_gamblers_together.append(std(protocolInstance.priceFinal_gamblers))

            averageSocialWelfare_gamblers_together.append(mean(protocolInstance.allSatisfactions_gamblers))
            stdSocialWelfare_gamblers_together.append(std(protocolInstance.allSatisfactions_gamblers))

            averageSocialWelfare2_gamblers_together.append(mean(protocolInstance.allSatisfactions2_gamblers))
            stdSocialWelfare2_gamblers_together.append(std(protocolInstance.allSatisfactions2_gamblers))

        else:
            averageConsumption_gamblers_together.append(nan)
            stdConsumption_gamblers_together.append(nan)

            averagePrice_gamblers_together.append(nan)
            stdPrice_gamblers_together.append(nan)

            averageSocialWelfare_gamblers_together.append(nan)
            stdSocialWelfare_gamblers_together.append(nan)

            averageSocialWelfare2_gamblers_together.append(nan)
            stdSocialWelfare2_gamblers_together.append(nan)

        if single_write:
            post = dict(problemNumber=problemNumber,
                        protocolType=protocolType,
                        numberOfHouses_all=numberOfHouses,
                        numberOfHouses=numberOfHouses - number_of_gamblers,
                        numberOfHouses_gamblers=number_of_gamblers,
                        numberOfTarrifs=numberOfTarrifs,
                        meanGamblingCoefficient=meanGamblingCoefficient,
                        numberOfPriceIncreases=numberOfPriceIncreases_together[-1],
                        length=length_together[-1],
                        averageConsumption=averageConsumption_together[-1],
                        stdConsumption=stdConsumption_together[-1],
                        averagePrice=averagePrice_together[-1],
                        stdPrice=stdPrice_together[-1],
                        averageSocialWelfare=averageSocialWelfare_together[-1],
                        averageSocialWelfare2=averageSocialWelfare2_together[-1],
                        stdSocialWelfare=stdSocialWelfare_together[-1],
                        averageConsumption_gamblers=averageConsumption_gamblers_together[-1],
                        stdConsumption_gamblers=stdConsumption_gamblers_together[-1],
                        averagePrice_gamblers=averagePrice_gamblers_together[-1],
                        stdPrice_gamblers=stdPrice_gamblers_together[-1],
                        averageSocialWelfare_gamblers=averageSocialWelfare_gamblers_together[-1],
                        averageSocialWelfare2_gamblers=averageSocialWelfare2_gamblers_together[-1],
                        stdSocialWelfare_gamblers=stdSocialWelfare_gamblers_together[-1],
                        pricesFinal=protocolInstance.priceFinal,
                        valueFinal=protocolInstance.valueFinal,
                        consumptionFinal=protocolInstance.consumptionFinal,
                        consumptionFinal_gamblers=protocolInstance.consumptionFinal_gamblers,
                        pricesFinal_gamblers=protocolInstance.priceFinal_gamblers,
                        valueFinal_gamblers=protocolInstance.valueFinal_gamblers,
                        allSatisfactions_gamblers=protocolInstance.allSatisfactions_gamblers)

            db.posts.insert(post)

            if return_any:
                posts_list.append(post)

        problemNumber += 1

    if not single_write:
        # insert the post with the average results into the database

        post = dict(problemNumber=problemNumber,
                    protocolType=protocolType,
                    numberOfHouses_all=numberOfHouses,
                    numberOfHouses=numberOfHouses - number_of_gamblers,
                    numberOfHouses_gamblers=number_of_gamblers,
                    numberOfTarrifs=numberOfTarrifs,
                    meanGamblingCoefficient=meanGamblingCoefficient,
                    numberOfPriceIncreases=mean(numberOfPriceIncreases_together),
                    length=mean(length_together),
                    averageConsumption=mean(averageConsumption_together),
                    stdAverageConsumption=std(averageConsumption_together),
                    stdConsumption=mean(stdConsumption_together),
                    averagePrice=mean(averagePrice_together),
                    stdAveragePrice=std(averagePrice_together),
                    stdPrice=mean(stdPrice_together),
                    averageSocialWelfare=mean(averageSocialWelfare_together),
                    averageSocialWelfare2=mean(averageSocialWelfare2_together),
                    stdAverageSocialWelfare=std(averageSocialWelfare_together),
                    stdAverageSocialWelfare2=std(averageSocialWelfare2_together),
                    stdSocialWelfare=mean(stdSocialWelfare_together),
                    averageConsumption_gamblers=mean(averageConsumption_gamblers_together),
                    stdAverageConsumption_gamblers=std(averageConsumption_gamblers_together),
                    stdConsumption_gamblers=mean(stdConsumption_gamblers_together),
                    averagePrice_gamblers=mean(averagePrice_gamblers_together),
                    stdAveragePrice_gamblers=std(averagePrice_gamblers_together),
                    stdPrice_gamblers=mean(stdPrice_gamblers_together),
                    averageSocialWelfare_gamblers=mean(averageSocialWelfare_gamblers_together),
                    averageSocialWelfare2_gamblers=mean(averageSocialWelfare2_gamblers_together),
                    stdAverageSocialWelfare_gamblers=std(averageSocialWelfare_gamblers_together),
                    stdAverageSocialWelfare2_gamblers=std(averageSocialWelfare2_gamblers_together),
                    stdSocialWelfare_gamblers=mean(stdSocialWelfare_gamblers_together))

        db.posts.insert(post)
        if return_any:
            posts_list.append(post)

    #print 'finished one...'
    return posts_list


# cross product of lists in list
def cross(*sets):
    # return iterator
    wheels = map(iter, sets)  # wheels like in an odometer
    digits = [it.next() for it in wheels]
    while True:
        yield digits[:]
        for i in range(len(digits)-1, -1, -1):
            try:
                digits[i] = wheels[i].next()
                break
            except StopIteration:
                wheels[i] = iter(sets[i])
                digits[i] = wheels[i].next()
        else:
            break



def runExperimentsMP(protocolTypeList=['serial', 'average', 'tarrifs'], numberOfExperimentRuns=100,
                     numberOfHousesList=[50], numberOfTarrifsList=[4], meanGamblingCoefficientList=[0], number_of_gamblers_list=[0]):

    protocolTypeList_copy = []
    for protocol_type in protocolTypeList:
        if protocol_type != "tarrifs":
            protocolTypeList_copy.append(protocol_type)

    problems_iterator = cross([numberOfExperimentRuns], numberOfHousesList, [None], meanGamblingCoefficientList, protocolTypeList_copy, number_of_gamblers_list)
    problem_size = len(numberOfHousesList)*len(meanGamblingCoefficientList)*len(protocolTypeList_copy)*len(number_of_gamblers_list)


    if "tarrifs" in protocolTypeList:
        problems_iterator = chain(problems_iterator,
                                          cross([numberOfExperimentRuns], numberOfHousesList, numberOfTarrifsList, meanGamblingCoefficientList, ["tarrifs"], number_of_gamblers_list))

        problem_size += len(numberOfHousesList)*len(meanGamblingCoefficientList)*len(number_of_gamblers_list)*len(numberOfTarrifsList)

    # should I delete entire results db?
    #db = MongoClient(host='jernejzupancicpc.ijs.si', port=27017)['results_db_social2']
    #db.posts.remove()

    pool = mp.Pool(7)

    outputs = []
    percent = 0
    for i, _ in enumerate(pool.imap_unordered(runExperimentSingle, problems_iterator)):
    #for i, _ in enumerate(map(runExperimentSingle, problems_iterator)):
        #print 'startint'
        percent_new = int(100*i/problem_size)
        if not percent == percent_new:
            percent = percent_new
            sys.stderr.write('\rDone {0:02d}%\t['.format(percent) + int(percent/2) * '#' + (50-int(percent/2)) * '_' + ']')
        # store results
        # outputs.append(_)

    pool.close()
    pool.join()

    return outputs


if __name__ == "__main__":
    # runExperimentsMP(protocolTypeList=['serial', 'tarrifs', 'average'], numberOfExperimentRuns=1000,
    #               numberOfHousesList=[50, 500, 1000], numberOfTarrifsList=[2, 4, 8],
    #               meanGamblingCoefficientList=[0.1, 0.3, 0.6, 0.9], number_of_gamblers_list=[1, 10, 25])

    mean_sat = []
    mean_cons = []
    mean_cost = []
    revenue = []


    mean_sat_g = []
    mean_cons_g = []
    mean_cost_g = []

    random.seed(94)

    satisfactions_opt_means = []
    satisfactions_our_means = []
    satisfactions_1_means = []
    for _ in range(50):
        print _
        protocolInstance = Protocol(Problem(numberOfHouses=50,
                                            numberOfBlocks=20,
                                            maxDevices=10,
                                            meanGamblingCoefficient=1,
                                            nrOfGamblers=0),
                                    "serial")

        #protocolInstance.run()
        protocolInstance.nash_equilibrium()
        #protocolInstance.all_rounds_nash_equilibrium()

        consumption_1 = protocolInstance.consumptionFinal
        prices_1 = protocolInstance.priceFinal
        satisfactions_1 = protocolInstance.allSatisfactions2

        costs_1 = [p*c for p, c in zip(prices_1, consumption_1)]
        #print "Revenue: ", sum(costs), "({})".format(mean(costs))

        mean_sat.append(mean(satisfactions_1))
        mean_cons.append(mean(consumption_1))
        mean_cost.append(mean(costs_1))



        consumption_g_1 = protocolInstance.consumptionFinal_gamblers
        prices_g_1 = protocolInstance.priceFinal_gamblers
        satisfactions_g_1 = protocolInstance.allSatisfactions2_gamblers

        costs_g_1 = [p*c for p, c in zip(prices_g_1, consumption_g_1)]
        #print "Revenue: ", sum(costs), "({})".format(mean(costs))

        mean_sat_g.append(mean(satisfactions_g_1))
        mean_cons_g.append(mean(consumption_g_1))
        mean_cost_g.append(mean(costs_g_1))

        cons_levels_1 = []
        for house in protocolInstance.problem.houseList:
            cons_levels_1.append(house.consumptionLevel)
        # ?????????????????????????????????????????????????????????

        protocolInstance.reset_variables()

        #protocolInstance.run()
        #protocolInstance.nash_equilibrium()
        protocolInstance.all_rounds_nash_equilibrium()

        consumption_opt = protocolInstance.consumptionFinal
        prices_opt = protocolInstance.priceFinal
        satisfactions_opt = protocolInstance.allSatisfactions2

        costs_opt = [p*c for p, c in zip(prices_opt, consumption_opt)]
        #print "Revenue: ", sum(costs), "({})".format(mean(costs))

        consumption_g_opt = protocolInstance.consumptionFinal_gamblers
        prices_g_opt = protocolInstance.priceFinal_gamblers
        satisfactions_g_opt = protocolInstance.allSatisfactions2_gamblers

        costs_g_opt = [p*c for p, c in zip(prices_g_opt, consumption_g_opt)]
        #print "Revenue: ", sum(costs), "({})".format(mean(costs))
        cons_levels_opt = []
        for house in protocolInstance.problem.houseList:
            cons_levels_opt.append(house.consumptionLevel)
        # ?????????????????????????????????????????????????????????

        protocolInstance.reset_variables()

        protocolInstance.run()
        #protocolInstance.nash_equilibrium()
        #protocolInstance.all_rounds_nash_equilibrium()

        consumption_our = protocolInstance.consumptionFinal
        prices_our = protocolInstance.priceFinal
        satisfactions_our = protocolInstance.allSatisfactions2

        costs_our = [p*c for p, c in zip(prices_our, consumption_our)]
        #print "Revenue: ", sum(costs), "({})".format(mean(costs))

        consumption_g_our = protocolInstance.consumptionFinal_gamblers
        prices_g_our = protocolInstance.priceFinal_gamblers
        satisfactions_g_our = protocolInstance.allSatisfactions2_gamblers

        costs_g_our = [p*c for p, c in zip(prices_g_our, consumption_g_our)]
        #print "Revenue: ", sum(costs), "({})".format(mean(costs))
        cons_levels_our = []
        for house in protocolInstance.problem.houseList:
            cons_levels_our.append(house.consumptionLevel)



        sat_diff1 = [sat_opt - sat1 for sat_opt, sat1 in zip(satisfactions_opt, satisfactions_1)]
        sat_diff_our = [sat_opt - sat1 for sat_opt, sat1 in zip(satisfactions_opt, satisfactions_our)]

        xticks_ = [2 * i for i in range(50/2 + 1)]
        seaborn.set_style("whitegrid")
        plt.rcParams["figure.figsize"] = (40,30)
        f, (ax1, ax2, ax3) = plt.subplots(3, sharex=True)
        ax1.set_title("Satisfactions")
        ax1.plot(satisfactions_1, label="One step", alpha=0.8)
        ax1.plot(satisfactions_opt, label="Optimal", alpha=0.5, lw=7)
        ax1.plot(satisfactions_our, label="Ours", alpha=0.8)
        ax1.plot([0, 49], [mean(satisfactions_1), mean(satisfactions_1)], label="One step mean", alpha=0.5, linestyle="--")
        ax1.plot([0, 49], [mean(satisfactions_opt), mean(satisfactions_opt)], label="Optimal mean", alpha=0.5, lw=7, linestyle="--")
        ax1.plot([0, 49], [mean(satisfactions_our), mean(satisfactions_our)], label="Ours mean", alpha=0.5, linestyle="--")
        ax1.set_xticks(xticks_)
        ax1.legend()

        print "1: ", sum(satisfactions_1), mean(satisfactions_1)
        print "opt: ", sum(satisfactions_opt), mean(satisfactions_opt)
        print "our: ", sum(satisfactions_our), mean(satisfactions_our)

        satisfactions_1_means.append(mean(satisfactions_1))
        satisfactions_opt_means.append(mean(satisfactions_opt))
        satisfactions_our_means.append(mean(satisfactions_our))
        #plt.show()


        ax2.set_title("Consumptions")
        ax2.plot(consumption_1, label="One step", alpha=0.8)
        ax2.plot(consumption_opt, label="Optimal", alpha=0.3, lw=4)
        ax2.plot(consumption_our, label="Ours", alpha=0.8)
        ax2.set_xticks(xticks_)
        ax2.legend()
        #plt.show()


        ax3.set_title("Prices")
        ax3.plot(prices_1, label="One step", alpha=0.8)
        ax3.plot(prices_opt, label="Optimal", alpha=0.3, lw=4)
        ax3.plot(prices_our, label="Ours", alpha=0.8)
        ax3.set_xticks(xticks_)
        ax3.legend()
        plt.show()


        # plt.hist(sat_diff1, alpha=0.6, label="Diff to one step", normed=True)
        # plt.hist(sat_diff_our, alpha=0.6, label="Diff to ours", normed=True)
        # plt.legend()
        # plt.show()
        # plt.hist(satisfactions)
        # plt.vlines(satisfactions_g, 0, 50, "r", lw=4)
        # plt.title("Satisfactions")
        # plt.show()

        # plt.hist(consumption)
        # plt.vlines(consumption_g, 0, 50, "r", lw=4)
        # plt.title("Consumptions")
        # plt.show()
        #
        # plt.hist(prices)
        # plt.vlines(prices_g, 0, 50, "r", lw=4)
        # plt.title("Prices")
        # plt.show()

    # print "\nMean satisfactions: ", mean(mean_sat), std(mean_sat)
    # print "Mean consumptions: ", mean(mean_cons), std(mean_cons)
    # print "Mean costs: ", mean(mean_cost), std(mean_cost)
    #
    # print "\nMean satisfactions_g: ", mean(mean_sat_g), std(mean_sat_g)
    # print "Mean consumptions_g: ", mean(mean_cons_g), std(mean_cons_g)
    # print "Mean costs_g: ", mean(mean_cost_g), std(mean_cost_g)

    # plt.hist(mean_sat_g)
    # plt.title("Satisfactions")
    # plt.show()
    #
    # plt.hist(mean_cons_g)
    # plt.title("Consumptions")
    # plt.show()
    #
    # plt.hist(mean_cost_g)
    # plt.title("Costs")
    # plt.show()

    # plot satisfaction means differences
    # plt.rcParams["figure.figsize"] = (40, 12)
    # plt.plot(satisfactions_1_means, label="One step")
    # plt.plot(satisfactions_opt_means, label="Opt")
    # plt.plot(satisfactions_our_means, label="Our")
    # plt.legend()
    # plt.show()
    #
    # plt.hist([opt - one for opt, one in zip(satisfactions_opt_means, satisfactions_1_means)], alpha=0.6, label="Diff to one")
    # plt.hist([opt - our for opt, our in zip(satisfactions_opt_means, satisfactions_our_means)], alpha=0.6, label="Diff to our")
    # plt.legend()
    # plt.show()

    print "Diff to 1 (max, min) ", max([opt - one for opt, one in zip(satisfactions_opt_means, satisfactions_1_means)]), min([opt - one for opt, one in zip(satisfactions_opt_means, satisfactions_1_means)])
    print "Diff to ours (max, min)", max([opt - our for opt, our in zip(satisfactions_opt_means, satisfactions_our_means)]), min([opt - our for opt, our in zip(satisfactions_opt_means, satisfactions_our_means)])

    import json
    to_dump = {}
    to_dump["optimal"] = satisfactions_opt_means
    to_dump["one_shot"] = satisfactions_1_means
    to_dump["ours"] = satisfactions_our_means
    with open("rez1.json", "w") as dmp_f:
        json.dump(to_dump, dmp_f)

    # rezultati = runExperimentsMP(protocolTypeList=['serial','tarrifs','average'], numberOfExperimentRuns=10,
    #                numberOfHousesList=[50], numberOfTarrifsList=[2, 4, 8],
    #                meanGamblingCoefficientList=[0.1, 0.3, 0.6], number_of_gamblers_list=[1])

    # rezultati = runExperimentsMP(meanGamblingCoefficientList=[0.1*i for i in range(5)], numberOfExperimentRuns=10000, number_of_gamblers_list=[1])
