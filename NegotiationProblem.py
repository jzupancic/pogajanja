__author__ = 'JernejZ'

import random

from Negotiator import Negotiator
from SmartHouse import SmartHouse

class Problem:
    def __init__(self, numberOfHouses=100, minDevices=3, maxDevices=5, minConsumption=0.1, maxConsumption=1.5,
                 meanPrice=0.15, deviationPrice=0.075, nrOfGamblers=1, meanGamblingCoefficient=0,
                 deviationGamblingCoefficient=0, meanBlockAdd=4, devBlockAdd=2, deltaPrice=1, numberOfBlocks=50,
                 numberOfTarrifs=4):
        self.numberOfHouses = numberOfHouses
        self.minDevices = minDevices
        self.maxDevices = maxDevices
        self.minConsumption = minConsumption
        self.maxConsumption = maxConsumption
        self.meanPrice = meanPrice
        self.deviationPrice = deviationPrice
        self.numberOfGamblers = nrOfGamblers
        self.meanGamblingCoefficient = meanGamblingCoefficient
        self.deviationGamblingCoefficient = deviationGamblingCoefficient
        self.meanBlockAdd = meanBlockAdd
        self.devBlockAdd = devBlockAdd
        self.deltaPrice = deltaPrice
        self.numberOfBlocks = numberOfBlocks
        self.numberOfTarrifs = numberOfTarrifs

        self.gamblers = []
        self.nonGamblers = []
        self.houseList = self.newHouseList()
        self.negotiator = self.newNegotiator()

    def newHouseList(self):
        houseList = []
        gamblersList = []
        nonGamblersList = []
        minDevices = self.minDevices
        maxDevices = self.maxDevices
        minConsumption = self.minConsumption
        maxConsumption = self.maxConsumption
        meanPrice = self.meanPrice
        deviationPrice = self.deviationPrice
        meanGamblingCoefficient = self.meanGamblingCoefficient
        deviationGamblingCoefficient = self.deviationGamblingCoefficient

        for i in xrange(self.numberOfGamblers):
            # numberOfDevices = random.randint(minDevices, maxDevices)
            # consumptionLst = [random.uniform(minConsumption, maxConsumption) for _ in range(numberOfDevices)]
            # priceLst = [abs(random.gauss(meanPrice, deviationPrice)) for _ in range(numberOfDevices)]
            nr_of_devices = random.randint(1, maxDevices)
            consumptions = [random.expovariate(1.0/100) for _ in range(nr_of_devices)]
            prices = sorted([random.expovariate(1.0/0.00020) for _ in range(nr_of_devices)], reverse=True)

            newHouse = SmartHouse(i, consumptions, prices, gamblingCoefficient=abs(
                random.gauss(meanGamblingCoefficient, deviationGamblingCoefficient)))
            houseList.append(newHouse)
            gamblersList += [newHouse]

        for i in xrange(self.numberOfHouses - self.numberOfGamblers):
            # TODO: change to previous
            # numberOfDevices = random.randint(minDevices, maxDevices)
            # consumptionLst = [random.uniform(minConsumption, maxConsumption) for _ in range(numberOfDevices)]
            # priceLst = [abs(random.gauss(meanPrice, deviationPrice)) for _ in range(numberOfDevices)]
            # --------------------------------------------------
            nr_of_devices = random.randint(1, maxDevices)
            consumptions = [random.expovariate(1.0/100) for _ in range(nr_of_devices)]
            prices = sorted([random.expovariate(1.0/0.00020) for _ in range(nr_of_devices)], reverse=True)


            newHouse = SmartHouse(i, consumptions, prices, gamblingCoefficient=0)
            houseList.append(newHouse)
            nonGamblersList += [newHouse]

        self.gamblers = gamblersList
        self.nonGamblers = nonGamblersList

        return houseList

    def newNegotiator(self):
        numberOfHouses = self.numberOfHouses
        minConsumption = self.minConsumption
        meanBlockAdd = self.meanBlockAdd
        devBlockAdd = self.devBlockAdd
        numberOfBlocks = self.numberOfBlocks

        # TODO: change generation of the blocks
        # blockSizes = [
        #     numberOfHouses * minConsumption * (1 + abs(random.gauss(meanBlockAdd, devBlockAdd))) for
        #     _ in xrange(numberOfBlocks)]
        # blockSizes.append(numberOfHouses * self.maxConsumption * self.maxDevices)
        #
        # meanPrice = self.meanPrice
        # deltaPrice = self.deltaPrice
        # deviationPrice = self.deviationPrice
        # blockPrices = [abs(random.gauss(meanPrice * (1 + deltaPrice), deviationPrice)) for _ in
        #                xrange(numberOfBlocks + 1)]
        # blockPrices = sorted(blockPrices)
        # ----------------------------------------------
        block_sizes = [random.expovariate(1.0/2000) for _ in range(numberOfBlocks-1)]
        block_sizes.append(999999999999999)
        block_prices = sorted([random.expovariate(1.0/0.0002) for _ in range(numberOfBlocks)])

        coordinator = Negotiator(block_sizes, block_prices, self.numberOfTarrifs)

        return coordinator

    def addHouse(self, newHouse):
        self.houseList.append(newHouse)
        self.numberOfHouses += 1

