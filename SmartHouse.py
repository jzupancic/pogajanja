__author__ = 'JernejZ'

import random

import matplotlib.pyplot as plt

class SmartHouse:
    def __init__(self, index, devicesConsumption, devicesPricesNegotiation, devicesPricesRenunciation=None,
                 gamblingCoefficient=0):
        self.index = index
        if not devicesPricesRenunciation:
            devicesPricesRenunciation = devicesPricesNegotiation
        self.devicesConsumption, self.devicesPricesNegotiation, self.devicesPricesRenunciation = self.orderDevices(
            devicesConsumption, devicesPricesNegotiation, devicesPricesRenunciation)
        self.gamblingCoefficient = gamblingCoefficient

        self.devicesUsed = [True for _ in devicesConsumption]
        self.isSatisfied = True
        self.comfort = None
        self.consumptionLevel = len(devicesConsumption)
        self.currentConsumption = sum(devicesConsumption[:self.consumptionLevel])
        self.currentMaxPrice = self.devicesPricesNegotiation[self.consumptionLevel - 1]
        self.consumptionTrack = []
        self.priceTrack = []

    def orderDevices(self, devicesConsumption, devicesPricesNegotiation, devicesPricesRenunciation):
        consumptionPrices = zip(devicesConsumption, devicesPricesNegotiation, devicesPricesRenunciation)
        consumptionPrices = sorted(consumptionPrices, key=lambda cp: cp[1], reverse=True)
        consumption, pricesNeg, pricesRen = zip(*consumptionPrices)

        return list(consumption), list(pricesNeg), list(pricesRen)

    def reportConsumptionNegotiation(self, price, counterINC=0):
        consumptionLevel = self.consumptionLevel
        currentMaxPrice = self.currentMaxPrice

        # if price > currentMaxPrice and consumptionLevel > 0:
        # # print "Should i gamble?"
        #     if random.random() < (1 - self.gamblingCoefficient):
        #         # lower consumption and expect a lower price
        #         consumptionLevel -= 1
        #
        #         currentMaxPrice = self.devicesPricesNegotiation[consumptionLevel - 1]
        #
        #     else:
        #         # gamble to get a lower price with the same consumption
        #         pass

        if consumptionLevel > 0:
            if random.random() < self.gamblingCoefficient:
                # 0.01 * (1.0 - len(self.priceTrack)/10.0) * self.getSatisfaction(sum(self.devicesConsumption), self.devicesPricesNegotiation[-1]) < \
                # if self.getSatisfaction(sum(self.devicesConsumption[:consumptionLevel]), price) + \
                #                 5 * (1.0 - len(self.priceTrack) / 10.0) < \
                #                 self.getSatisfaction(sum(self.devicesConsumption[:consumptionLevel - 1]), price):
                #     consumptionLevel -= 1
                #     currentMaxPrice = self.devicesPricesNegotiation[consumptionLevel - 1]
                if len(self.priceTrack) < 2:
                    pass
                else:
                    delta_price = self.priceTrack[-2] - self.priceTrack[-1]
                    expected_price = price - delta_price
                    if self.getSatisfaction(sum(self.devicesConsumption[:consumptionLevel]), expected_price) < \
                            self.getSatisfaction(sum(self.devicesConsumption[:consumptionLevel - 1]), expected_price):
                        consumptionLevel -= 1
                        currentMaxPrice = self.devicesPricesNegotiation[consumptionLevel - 1]

            elif self.getSatisfaction(sum(self.devicesConsumption[:consumptionLevel]), price) < \
                    self.getSatisfaction(sum(self.devicesConsumption[:consumptionLevel - 1]), price):
                consumptionLevel -= 1
                currentMaxPrice = self.devicesPricesNegotiation[consumptionLevel - 1]

                   # elif random.random() > (1 - self.gamblingCoefficient):
                   #     # gamble with higher demand than previous round
                   #     if consumptionLevel < len(self.devicesConsumption)-1:
                   #         consumptionLevel += 1
                   #         self.devicesUsed[consumptionLevel] = True
                   #         currentMaxPrice = self.devicesPricesNegotiation[consumptionLevel - 1]
                   #         counterINC += 1
                   #         #print "I have increased consumption!!!!"
                   #         #print 100*"#"+"\n"

        if consumptionLevel == 0:
            currentMaxPrice = 0

        if consumptionLevel < sum(self.devicesUsed):
            self.devicesUsed[consumptionLevel - 1] = False
        else:
            self.devicesUsed[consumptionLevel - 1] = True

        self.consumptionLevel = consumptionLevel
        self.currentMaxPrice = currentMaxPrice
        self.currentConsumption = sum(self.devicesConsumption[:consumptionLevel])
        self.consumptionTrack.append(self.currentConsumption)
        self.priceTrack.append(price)

        return self.currentConsumption, counterINC


    def reportConsumptionRenunciation(self):
        if self.consumptionLevel == 1:
            return self.currentConsumption
        else:
            return sum(self.devicesConsumption[:self.consumptionLevel])

    def acceptsRenunciation(self, renunciationPrice):
        if renunciationPrice <= self.devicesPricesRenunciation[self.consumptionLevel - 2]:
            self.currentConsumption = sum(self.devicesConsumption[:self.consumptionLevel - 1])
            return True
        return False


    def getsRenunciationPrice(self, renunciationPrice):
        self.currentPrice = renunciationPrice


    def setTrackingRight(self):
        pass


    def getSatisfaction(self, consumption, price):
        if consumption == 0:
            return 0

        utility = 0
        count_devices = 0
        consumption_i = 0

        while consumption_i < consumption:
            # print count_devices, consumption, sum(self.devicesConsumption[:count_devices]), self.devicesConsumption
            consumption_i += self.devicesConsumption[count_devices]
            max_price = self.devicesPricesNegotiation[count_devices]
            utility += (max_price - price) * self.devicesConsumption[count_devices]
            count_devices += 1

        max_utility = sum([cons * pr for cons, pr in zip(self.devicesConsumption, self.devicesPricesNegotiation)])

        return utility/max_utility


    def setSatisfaction(self):
        self.isSatisfied = self.currentMaxPrice - self.priceTrack[-1]
        self.isSatisfied2 = self.getSatisfaction(self.currentConsumption, self.priceTrack[-1])


    def plotHouse(self):
        fig, ax1 = plt.subplots()
        ax1.plot(self.consumptionTrack, 'ob-')
        ax1.set_xlabel('negotiation round')
        # Make the y-axis label and tick labels match the line color.
        ax1.set_ylabel('consumption', color='b')
        for tl in ax1.get_yticklabels():
            tl.set_color('b')

        ax2 = ax1.twinx()
        ax2.plot(self.priceTrack, '.r-')
        ax2.set_ylabel('price', color='r')
        for tl in ax2.get_yticklabels():
            tl.set_color('r')
        plt.show()


    def reset(self):
        self.devicesUsed[:] = [True for _ in self.devicesConsumption]
        self.comfort = None
        self.consumptionLevel = len(self.devicesConsumption)
        self.currentConsumption = sum(self.devicesConsumption[:self.consumptionLevel])
        self.currentMaxPrice = self.devicesPricesNegotiation[self.consumptionLevel - 1]
        self.consumptionTrack[:] = []
        self.priceTrack[:] = []
        self.isSatisfied = 0
        self.isSatisfied2 = 0
