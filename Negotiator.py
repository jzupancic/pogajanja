__author__ = 'JernejZ'

from CostFunction import CostFunction

class Negotiator:
    def __init__(self, blockSizeList, priceList, numberOfTarrifs=4):
        self.blockSizes, self.blockPrices = self.orderBlocks(blockSizeList, priceList)
        self.numberOfTarrifs = numberOfTarrifs
        # self.currentConsumptions = None
        # self.previousConsumptions = None
        self.currentPrices = None
        self.costFunction = CostFunction(self.blockSizes, self.blockPrices, self.numberOfTarrifs)

    def orderBlocks(self, blockSizes, blockPrices):
        blockSP = sorted(zip(blockSizes, blockPrices), key=lambda x: x[1])
        return zip(*blockSP)

    def computePricesNegotiation(self, consumptions):
        return self.costFunction.computeMultiplePrice(consumptions)

    def computePricesAverage(self, consumptions):
        return self.costFunction.computeMultiplePriceAverage(consumptions)

    def computePricesTarrifs(self, consumptions):
        return self.costFunction.computeMultiplePriceAverageTarrifs(consumptions)

    def set_new_consumptions(self, new_consumptions):
        self.currentConsumptions, self.previousConsumptions = new_consumptions, self.currentConsumptions

    def plotCostFunction(self):
        coordx = [sum(self.blockSizes[:i]) for i in xrange(len(self.blockSizes))]
        coordx.append(coordx[-1] + self.blockSizes[-1])

        coordy = [0]
        for indexX, priceY in enumerate(self.blockPrices):
            coordy += [coordy[-1] + 1.0 * priceY * self.blockSizes[indexX]]

        return coordx, coordy
